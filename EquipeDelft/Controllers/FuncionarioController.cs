﻿using EquipeDelft.Models.Entidades;
using EquipeDelft.Repositorios;
using EquipeDelft.ViewModel.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EquipeDelft.Controllers
{
    public class FuncionarioController : Controller
    {

        ProdutosEFRepository repositorio = new ProdutosEFRepository();

        // GET: Funcionario
        public ActionResult Index()
        {
            return View();
        }


        //1
        public ActionResult CadastroFuncionario()
        {
            ViewBag.Message = "Cadastro de funcionários";

            return View();
        }

        //4 Funcionário
        [HttpGet]
        public ActionResult CadastroEquipamento()
        {
            ViewBag.Message = "Cadastro de equipamentos";

            return View();
        }

        [HttpPost]
        public ActionResult CadastroEquipamento(CadastroProdutoForm formulario)
        {
            Produto produto = new Produto(formulario.Nome, formulario.Descricao, formulario.Fornecedor, formulario.Foto);
            //Usuario user = new Usuario(formulario.Email, formulario.Senha, formulario.Nome, formulario.Sobrenome, formulario.Genero, formulario.Cpf, formulario.Endereco, formulario.Complemento, formulario.Bairro, formulario.Cidade, formulario.Estado, formulario.Cep, formulario.Telefone, formulario.Celular, formulario.Foto);
            if (!ModelState.IsValid) return View(formulario);

            repositorio.Salvar(produto);

            return RedirectToAction("Index", "Home");
        }




        //6
        public ActionResult GerenciarEquipamentos()
        {
            ViewBag.Message = "Gerenciamento de equipamentos";

            return View();
        }

        //9 Funcionário Gerente?
        public ActionResult ValidarContrato()
        {
            ViewBag.Message = "Validação de contratos";

            return View();
        }

        //12 Funcionário Gerente
        public ActionResult GestaoDeFuncionarios()
        {
            ViewBag.Message = "Acompanhamento";

            return View();
        }

        //14 Funcionário Gerente
        public ActionResult ConsultarRelatorio()
        {
            ViewBag.Message = "Consultar Relatório";

            return View();
        }

        //15 FUncionário Gerente
        public ActionResult ValidarRelatorio()
        {
            ViewBag.Message = "Validar Relatório";

            return View();
        }
    }
}