﻿using EquipeDelft.Models.Entidades;
using EquipeDelft.Repositorios;
using EquipeDelft.ViewModel.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EquipeDelft.Controllers
{
    public class UsuarioController : Controller
    {
        UsuariosEFRepository repositorio = new UsuariosEFRepository();
        // GET: Usuario
        public ActionResult Index()
        {
            
                ProdutosEFRepository repository = new ProdutosEFRepository();
                var produtos = repository.ObterTodos();

            
            return View(produtos);
        }

        //2 Usuario
        [HttpGet]
        public ActionResult CadastroCliente()
        {
            ViewBag.Message = "Cadastro de clientes";

            return View();
        }

        [HttpPost]
        public ActionResult CadastroCliente(FormularioCadastro formulario)
        {
            Usuario user = new Usuario(formulario.Email, formulario.Senha, formulario.Nome);
            //Usuario user = new Usuario(formulario.Email, formulario.Senha, formulario.Nome, formulario.Sobrenome, formulario.Genero, formulario.Cpf, formulario.Endereco, formulario.Complemento, formulario.Bairro, formulario.Cidade, formulario.Estado, formulario.Cep, formulario.Telefone, formulario.Celular, formulario.Foto);
            if (!ModelState.IsValid) return View(formulario);

            repositorio.Salvar(user);
            Session.Add("usuarioLogado", user.Email);
            return RedirectToAction("Index", "Home");
        }




       


        //7 Usuário
        public ActionResult HistoricoDeCompras()
        {
            ViewBag.Message = "Seu histórico de compras";

            return View();
        }

        //8 Usuário
        public ActionResult ExtenderContrato()
        {
            ViewBag.Message = "Extensão de contratos";

            return View();
        }

        //10 Usuário
        [HttpGet]
        public ActionResult FazerPedido()
        {
            ViewBag.Message = "Pedidos";
            if (Session["carrinho"] != null)
            {
                Carrinho c = Session["carrinho"] as Carrinho;
                return View(c.produtos);
            }

            return View(new Carrinho().produtos);
        }

        [HttpPost]
        public ActionResult FazerPedido(FormCollection form)
        {
            if(Session["usuarioLogado"] == null)
                return RedirectToAction("Login", "Home");

            //Locacao
            Locacao locacao = new Locacao();
            LocacoesEFRepository repository = new LocacoesEFRepository();

            UsuariosEFRepository repositoryUser = new UsuariosEFRepository();
            String email = Session["usuarioLogado"] as String;

            locacao.Id_Usuario = repositoryUser.Obter(email).Id;
            locacao.Inicio = Convert.ToDateTime(form["dataInicio"]);
            locacao.Fim = Convert.ToDateTime(form["dataFim"]);
            repository.Salvar(locacao);

            //Itens Locacao
            Carrinho cart = Session["carrinho"] as Carrinho;
            ItensEFRepository repositoryItens = new ItensEFRepository();

            int indice = 0;
            foreach (Locacao l in repository.obterTodas())
            {
                indice = l.Id;
            }

            foreach (Produto produto in cart.produtos)
            {
                Item item = new Item(indice, produto.Id);
                repositoryItens.salvar(item);
            }

            return RedirectToAction("Index", "Home");
        }



        //11 Usuário
        public ActionResult AcompanharLocacao()
        {
            ViewBag.Message = "Acompanhamento";

            return View();
        }

        //13 Usuário
        public ActionResult Contratos()
        {
            ViewBag.Message = "Contratos";
            if(Session["usuarioLogado"] == null)
                return RedirectToAction("Login", "Home");


            LocacoesEFRepository repository = new LocacoesEFRepository();
            UsuariosEFRepository users = new UsuariosEFRepository();

            int id = users.Obter(Session["usuarioLogado"] as String).Id;

            return View(repository.obterTodas());
            //return View(repository.obterTodasDeUmUsuario(id));
        }

        public ActionResult Carrinho() {
            ViewBag.Message = "Carrinho do usuário";
            if (Session["carrinho"] != null)
            {
                Carrinho c = Session["carrinho"] as Carrinho;
                return View(c.produtos);
            }

            return View(new Carrinho().produtos);
        }

        public ActionResult AdicionarCarrinho(int id) {
            ViewBag.Message = "Adicionado com sucesso";

            ProdutosEFRepository repository = new ProdutosEFRepository();


            //Criar carrinho caso não exista
            if (Session["carrinho"] == null)
            {
                Carrinho cart = new Carrinho();
                Session.Add("carrinho", cart);
            }



            Carrinho c = Session["carrinho"] as Carrinho;

            if (!c.existe(id)){
                c.adiconar(repository.Obter(id));
            }

            Session.Add("carrinho", c);


            return RedirectToAction("Carrinho", "Usuario");
        }
    }
}