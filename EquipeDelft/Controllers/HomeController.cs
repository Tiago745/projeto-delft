﻿using EquipeDelft.Models.Entidades;
using EquipeDelft.Repositorios;
using EquipeDelft.ViewModel.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EquipeDelft.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            

            var repositorio = new ProdutosEFRepository();
            var p = repositorio.ObterTodos();
            
            

            return View(p);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        
         
       

        //3  X
        [HttpGet]
        public ActionResult Login()
        {
            ViewBag.Message = "Login";

            return View();
        }

        [HttpPost]
        public ActionResult Login(FormularioLogin formulario)
        {
            if (!ModelState.IsValid) return View(formulario);
            else
            {
                UsuariosEFRepository repositorio = new UsuariosEFRepository();
                Usuario user = repositorio.Obter(formulario.Email);
                if (user == null)
                    return View(formulario);
                    if (user.Senha != formulario.Senha)
                    {
                        return View(formulario);
                    }
                    Session.Add("usuarioLogado", user.Email);
                    ViewBag.Usuario = repositorio;
            }
            return RedirectToAction("Index", "Home");
            //TempData["MensagemSucesso"] = "Usuário logado!";
        }

        //15 FUncionário Gerente
        public ActionResult ValidarRelatorio()
        {
            ViewBag.Message = "Validar Relatório";

            return View();
        }

        //Produto
        public ActionResult Produto(int id)
        {
            ViewBag.Message = "Produto Específico";


            var repositorio = new ProdutosEFRepository();
            Produto p = repositorio.Obter(id);
            ViewBag.Produto = p;

            return View();
        }

        public ActionResult Error(String mensagemDeErro) {
            ViewBag.Erro = "hello";

            return View(mensagemDeErro);
        }

    }
}