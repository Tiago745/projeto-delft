﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EquipeDelft.ViewModel.Usuario
{
    public class FormularioLogin
    {
        [DisplayName("E-mail")]
        [Required(ErrorMessage = "Este campo é obrigatório.")]
        [EmailAddress(ErrorMessage = "Deve ser informado um e-mail válido.")]
        [MaxLength(length: 50, ErrorMessage = "Este campo deve possuir, no máximo, 50 caracteres.")]
        public string Email { get; set; }

        [DisplayName("Senha")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Este campo é obrigatório.")]
        [MinLength(length: 6, ErrorMessage = "A senha deve conter ao menos 6 caracteres.")]
        public string Senha { get; set; }

    }
}