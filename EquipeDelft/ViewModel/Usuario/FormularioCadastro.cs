﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace EquipeDelft.ViewModel.Usuario
{
    public class FormularioCadastro
    {
        [DisplayName("E-mail")]
        [Required(ErrorMessage = "Este campo é obrigatório.")]
        [EmailAddress(ErrorMessage = "Deve ser informado um e-mail válido.")]
        [MaxLength(length: 50, ErrorMessage = "Este campo deve possuir, no máximo, 50 caracteres.")]
        public string Email { get; set; }
        
        [Required(ErrorMessage = "Este campo é obrigatório.")]
        [MinLength(6)]
        public String Senha { get; set; }
        
        [Required(ErrorMessage = "Este campo é obrigatório.")]
        [MaxLength(30)]
        public String Nome { get; set; }

        /*
        [MaxLength(50)]
        public String Sobrenome { get; set; }

        [DisplayName("Gênero")]
        [MaxLength(1)]
        public String Genero { get; set; }


        [Required(ErrorMessage = "Este campo é obrigatório.")]
        [MaxLength(14)]
        public String Cpf { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório.")]
        [MaxLength(25)]
        public String Endereco { get; set; }


        [Required(ErrorMessage = "Este campo é obrigatório.")]
        [MaxLength(8)]
        public String Complemento { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório.")]
        [MaxLength(25)]
        public String Bairro { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório.")]
        [MaxLength(25)]
        public String Cidade { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório.")]
        [MaxLength(2)]
        public String Estado { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório.")]
        [MaxLength(9)]
        public String Cep { get; set; }

        [MaxLength(20)]
        public String Telefone { get; set; }

        [MaxLength(20)]
        public String Celular { get; set; }


        public String Foto { get; set; }

        */
    }
}