﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EquipeDelft.ViewModel.Usuario
{
    public class FormularioLocacao
    {

        [DisplayName("Inicio")]
        [Required(ErrorMessage = "Este campo é obrigatório.")]
        public DateTime Inicio { get; private set; }

        [DisplayName("Fim")]
        [Required(ErrorMessage = "Este campo é obrigatório.")]
        public DateTime Fim { get; private set; }
        
        
    }
}