﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace EquipeDelft.ViewModel.Produto
{
    public class CadastroProdutoForm
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(40)]
        public String Nome { get; set; }

        [Required]
        [MaxLength(280)]
        public String Descricao { get; set; }

        [MaxLength(22)]
        public String Fornecedor { get; set; }


        public String Foto { get; set; }

        [Required]
        public Boolean disponivel { get; set; }
    }
}