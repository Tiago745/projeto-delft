﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EquipeDelft.Models.Entidades
{
    [Table(name: "Produtos")]
    public class Produto
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(40)]
        public String Nome { get; set; }

        [Required]
        [MaxLength(280)]
        public String Descricao { get; set; }

        [MaxLength(22)]
        public String Fornecedor { get; set; }


        public string Foto { get; set; }

        [Required]
        public Boolean disponivel { get; set; }


        public Produto(String nome, String descricao, String fornecedor, String foto)
        {
            this.Nome = nome;
            this.Descricao = descricao;
            this.Fornecedor = fornecedor;
            this.Foto = foto;
            this.disponivel = true;
        }

        public Produto() { }
    }
}