﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EquipeDelft.Models.Entidades
{
    [Table(name: "Usuarios")]
    public class Usuario
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public String Email { get; set; }


        [Required]
        [MinLength(6)]
        public String Senha { get; set; }

        [Required]
        [MaxLength(30)]
        public String Nome { get; set; }

        
        [MaxLength(50)]
        public String Sobrenome { get; set; }

        
        [MaxLength(1)]
        public String Genero { get; set; }

            
        [MaxLength(14)]
        public String Cpf { get; set; }

        
        [MaxLength(25)]
        public String Endereco { get; set; }


        
        [MaxLength(8)]
        public String Complemento { get; set; }

        
        [MaxLength(25)]
        public String Bairro { get; set; }

        
        [MaxLength(25)]
        public String Cidade { get; set; }

        
        [MaxLength(2)]
        public String Estado { get; set; }

        
        [MaxLength(9)]
        public String Cep { get; set; }

        [MaxLength(20)]
        public String Telefone { get; set; }

        [MaxLength(20)]
        public String Celular { get; set; }


        public String Foto{ get; set; }


        

        public Usuario(String email, String senha, String nome) {
            this.Email = email;
            this.Senha = senha;
            this.Nome = nome;
        }

        public Usuario() {

        }
        

        /*
        public Usuario(string email, string senha, string nome, string sobrenome, string genero, string cpf, string endereco, string complemento, string bairro, string cidade, string estado, string cep, string telefone, string celular, string foto)
        {
            Sobrenome = sobrenome;
            Genero = genero;
            Cpf = cpf;
            Endereco = endereco;
            Complemento = complemento;
            Bairro = bairro;
            Cidade = cidade;
            Estado = estado;
            Cep = cep;
            Telefone = telefone;
            Celular = celular;
            Foto = foto;
        }
       */
    }
}