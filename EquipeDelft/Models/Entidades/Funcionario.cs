﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

using System.Linq;
using System.Web;

namespace EquipeDelft.Models.Entidades
{


    [Table(name: "Funcionarios")]
    public class Funcionario
    {
        [Key]
        public int Id { get; set; }


        [Required]
        [MaxLength(50)]
        public String Email { get; set; }

        [Required]
        [MinLength(6)]
        public String Senha { get; set; }

        [Required]
        [MaxLength(30)]
        public String Nome { get; set; }

        [MaxLength(50)]
        public String Sobrenome { get; set; }

        [MaxLength(1)]
        public String Genero { get; set; }

        [Required]
        [MaxLength(14)]
        public String Cpf { get; set; }

        [MaxLength(15)]
        public String Funcao { get; set; }

        [MaxLength(20)]
        public String CarteiraDeTrabalho { get; set; }

        [MaxLength(20)]
        public String Telefone { get; set; }

        [MaxLength(20)]
        public String Celular { get; set; }
        

        public String Foto { get; set; }

        
        public Funcionario( String email, String senha, String nome, String cpf) {
            this.Nome = nome;
            this.Email = email;
            this.Senha = senha;
            this.Cpf = cpf;
        }

    }
}