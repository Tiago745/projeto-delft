﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

using System.Linq;
using System.Web;
namespace EquipeDelft.Models.Entidades
{

    [Table(name: "Locacoes")]
    public class Locacao
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public DateTime Inicio { get;  set; }

        public DateTime Fim { get;  set; }

        //public List<Produto> Produtos { get; set; }

        [Required]
        public int Id_Usuario { get; set; }

        public Locacao(){
        }
    }
}