﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EquipeDelft.Models.Entidades
{
    public class Carrinho
    {
        public List<Produto> produtos { get; set; }  = new List<Produto>();

        public Carrinho()
        {
            
        }


        public void adiconar(Produto produto)
        {
            produtos.Add(produto);
        }

        public void remover(int posicao)
        {
            produtos.RemoveAt(posicao);
        }

        public bool existe(int id)
        {
            foreach (Produto p in produtos) {
                if (p.Id == id)
                    return true;
            }
            return false;
        }
    }
}