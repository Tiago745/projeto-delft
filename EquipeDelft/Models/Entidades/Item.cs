﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EquipeDelft.Models.Entidades
{
    [Table(name: "Itens")]
    public class Item
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int Id_Locacao { get; set; }

        [Required]
        public int Id_Produto { get; set; }

        public Item() {
        }

        public Item(int Id_Locacao, int Id_Produto) {
            this.Id_Locacao = Id_Locacao;
            this.Id_Produto = Id_Produto;
        }
    }
}