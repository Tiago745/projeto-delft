﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EquipeDelft.Models.Entidades;

namespace EquipeDelft.Repositorios
{
    public class LocacoesEFRepository : ILocacoesRepository
    {
        

        public Locacao obter(int id)
        {
            throw new NotImplementedException();

            using (var db = new EquipeDelftContext())
            {
                var locacao = db.Locacoes.SingleOrDefault(l => l.Id == id);

                return locacao;
            }
        }

        public IEnumerable<Locacao> obterTodas()
        {
            using (var db = new EquipeDelftContext())
            {
                var resultado = db.Locacoes
                    .OrderBy(locacao => locacao.Id);

                return resultado.ToList();
            }
        }

        public IEnumerable<Locacao> obterTodasDeUmUsuario(int idUsuario)
        {
            using (var db = new EquipeDelftContext())
            {
                var resultado = db.Locacoes
                    .Where(locacao => locacao.Id == idUsuario);

                return resultado.ToList();
            }
        }

        public void Salvar(Locacao locacao)
        {
            using(var db = new EquipeDelftContext())
            {
                db.Locacoes.Add(locacao);
                db.SaveChanges();
            }
            //db.Entry(usuario).State = usuario.Id == 0 ? System.Data.Entity.EntityState.Added : System.Data.Entity.EntityState.Modified;

        }
    }
}