﻿using EquipeDelft.Models.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquipeDelft.Repositorios
{
    interface ILocacoesRepository
    {
        IEnumerable<Locacao> obterTodas();
        Locacao obter(int id);
        IEnumerable<Locacao> obterTodasDeUmUsuario(int idUsuario);
        void Salvar(Locacao locacao);
    }
}
