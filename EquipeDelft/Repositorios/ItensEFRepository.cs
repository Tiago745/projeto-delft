﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EquipeDelft.Models.Entidades;

namespace EquipeDelft.Repositorios
{
    public class ItensEFRepository : IItensRepository
    {
        public IEnumerable<Item> ObterTodos()
        {
            using (var db = new EquipeDelftContext())
            {
                var resultado = db.Itens
                    .OrderBy(item => item.Id);

                return resultado.ToList();
            }
        }

        public IEnumerable<Item> ObterTodosPorLocacao(int id_locacao)
        {
            using(var db = new EquipeDelftContext())
            {
                var resultado = db.Itens
                    .Where(item => item.Id_Locacao == id_locacao)
                    .OrderBy(item => item.Id);

                return resultado.ToList();
            }
        }

        public void salvar(Item item)
        {
            using (var db = new EquipeDelftContext())
            {
                db.Itens.Add(item);
                db.SaveChanges();
            }
        }
    }
}