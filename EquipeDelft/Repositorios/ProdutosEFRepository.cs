﻿using EquipeDelft.Models.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EquipeDelft.Repositorios
{
    public class ProdutosEFRepository : IProdutosRepository
    {
        public Produto Obter(int id)
        {
            using (var db = new EquipeDelftContext())
            {
                //db.Produtos.ToList().Count
                var produto = db.Produtos.SingleOrDefault(q => q.Id == id);

                return produto;
            }
        }

        public IEnumerable<Produto> ObterTodos()
        {
            using (var db = new EquipeDelftContext())
            {
                var resultado = db.Produtos
                    .OrderBy(produto => produto.Nome);

                return resultado.ToList();
            }
        }

        public void Salvar(Produto produto)
        {
            using (var db = new EquipeDelftContext())
            {

                if (db.Produtos.SingleOrDefault(q => q.Nome == produto.Nome) == null)
                {
                    db.Entry(produto).State = produto.Id == 0 ? System.Data.Entity.EntityState.Added : System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }

                else
                {
                    throw new Exception("Produto com mesmo nome já está cadastrado");
                }




            }
            //db.Entry(usuario).State = usuario.Id == 0 ? System.Data.Entity.EntityState.Added : System.Data.Entity.EntityState.Modified;

        }
    }
}