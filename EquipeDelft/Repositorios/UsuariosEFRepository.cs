﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EquipeDelft.Models.Entidades;

namespace EquipeDelft.Repositorios
{
    public class UsuariosEFRepository : IUsuariosRepository
    {
        public Usuario Obter(int id)
        {
            using (var db = new EquipeDelftContext())
            {
                var usuario = db.Usuarios.SingleOrDefault(q => q.Id == id);

                return usuario;
            }
        }

        public Usuario Obter(String email)
        {
            using (var db = new EquipeDelftContext())
            {
                var usuario = db.Usuarios.SingleOrDefault(q => q.Email == email);

                return usuario;
            }
        }



        public IEnumerable<Usuario> ObterTodos()
        {
            using (var db = new EquipeDelftContext())
            {
                var resultado = db.Usuarios
                    .OrderBy(usuario => usuario.Nome);

                return resultado.ToList();
            }
        }

        public void Salvar(Usuario usuario)
        {
            using (var db = new EquipeDelftContext())
            {

                if (db.Usuarios.SingleOrDefault(q => q.Email == usuario.Email) == null)
                {
                    db.Entry(usuario).State = usuario.Id == 0 ? System.Data.Entity.EntityState.Added : System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }

                else
                {
                    throw new Exception("E-mail já cadastrado");
                }
                    
                


                } 
                //db.Entry(usuario).State = usuario.Id == 0 ? System.Data.Entity.EntityState.Added : System.Data.Entity.EntityState.Modified;
                
            }
        }
    }
