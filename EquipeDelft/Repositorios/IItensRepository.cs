﻿using EquipeDelft.Models.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquipeDelft.Repositorios
{
    interface IItensRepository
    {
        void salvar(Item item);
        IEnumerable<Item> ObterTodos();
        IEnumerable<Item> ObterTodosPorLocacao(int id_locacao);

    }
}
