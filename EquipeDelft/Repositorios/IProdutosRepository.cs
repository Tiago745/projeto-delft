﻿using EquipeDelft.Models.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquipeDelft.Repositorios
{
    interface IProdutosRepository
    {
        Produto Obter(int id);
        IEnumerable<Produto> ObterTodos();
        void Salvar(Produto produto);


    }
}
