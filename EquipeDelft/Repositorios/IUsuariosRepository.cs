﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EquipeDelft.Models.Entidades;

namespace EquipeDelft.Repositorios
{
    public interface IUsuariosRepository 
    {

            Usuario Obter(int id);

            IEnumerable<Usuario> ObterTodos();
        
            void Salvar(Usuario user);
        

    }
}